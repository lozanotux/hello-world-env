# hello-world-env

A docker image for learning and testing with web apps and environment variables.

## Using with local docker

```
docker run -p 3000:3000 --rm hello-world-env:latest
```

```
GET http://localhost:3000/
```

Prints

```
Hello there!... Message -->
```

## Using with environment variables

```
docker run -p 3000:3000 --rm -e "MESSAGE=cool env" hello-world-env:latest
```

```
GET http://localhost:3000/
```

Prints

```
Hello there!... Message --> cool env
```
